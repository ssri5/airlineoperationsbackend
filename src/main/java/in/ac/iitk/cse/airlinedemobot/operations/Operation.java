package in.ac.iitk.cse.airlinedemobot.operations;

import in.ac.iitk.cse.airlinedemobot.Parameters;

public interface Operation {

	public String executeOperation(Parameters parameters);
}
