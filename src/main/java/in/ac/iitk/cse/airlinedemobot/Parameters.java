package in.ac.iitk.cse.airlinedemobot;

import java.io.Serializable;
import java.lang.reflect.Field;

public class Parameters implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String bookingId;
	
	private String date;
	
	private String dest;
	
	private String email;
	
	private String flightNumber;
	
	private Double numberOfTickets;

	private String src;
	
	private String time;
	
	public void capitaliseAllStringParameters() throws Exception {
		for(Field f : Parameters.class.getDeclaredFields()) {
			if(f.getType().equals(String.class)) {
				Object val = f.get(this);
				if(val != null) {
					val = val.toString().toUpperCase();
					f.set(this, val);
				}
			}
		}
	}

	public String getBookingId() {
		return bookingId;
	}

	public String getDate() {
		return date;
	}

	public String getDest() {
		return dest;
	}

	public String getEmail() {
		return email;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public Double getNumberOfTickets() {
		return numberOfTickets;
	}

	public String getSrc() {
		return src;
	}

	public String getTime() {
		return time;
	}

	public void setBookingId(String bookingId) {
		int decimalPoint = -1;
		if(bookingId != null && (decimalPoint = bookingId.indexOf('.')) != -1)
			bookingId = bookingId.substring(0, decimalPoint);
		this.bookingId = bookingId;
	}

	public void setDate(String date) {
		if(date != null && date.trim().length() == 0)
			date = null;
		else
			date = date.substring(0, 10);
		this.date = date;
	}

	public void setDest(String dest) {
		if(dest != null && dest.trim().length() == 0)
			dest = null;
		this.dest = dest;
	}

	public void setEmail(String email) {
		if(email != null && email.trim().length() == 0)
			email = null;
		this.email = email;
	}

	public void setFlightNumber(String flightNumber) {
		if(flightNumber != null && flightNumber.trim().length() == 0)
			flightNumber = null;
		this.flightNumber = flightNumber;
	}

	public void setNumberOfTickets(Double numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}

	public void setSrc(String src) {
		if(src != null && src.trim().length() == 0)
			src = null;
		this.src = src;
	}

	public void setTime(String time) {
		if(time != null && time.trim().length() == 0)
			time = null;
		this.time = time;
	}
	
	@Override
	public String toString() {
		return "Parameters [date=" + date + ", dest=" + dest + ", email=" + email + ", flightNumber=" + flightNumber
				+ ", numberOfTickets=" + numberOfTickets + ", src=" + src + ", time=" + time + ", bookingId="
				+ bookingId + "]";
	}
}
