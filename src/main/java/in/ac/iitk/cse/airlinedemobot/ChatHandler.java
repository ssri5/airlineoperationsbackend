package in.ac.iitk.cse.airlinedemobot;

import org.json.JSONException;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import in.ac.iitk.cse.airlinedemobot.operations.BookFlightOperation;
import in.ac.iitk.cse.airlinedemobot.operations.CancelFlightOperation;
import in.ac.iitk.cse.airlinedemobot.operations.SearchFlightOperation;
import in.ac.iitk.cse.airlinedemobot.operations.ShowFlightsOperation;

public class ChatHandler implements RequestHandler<Request, String> {

	private static String executeRequestedOperation(String operation, Parameters parameters, LambdaLogger logger) {
		
		try {
			if(operation.equals("search")) {
				SearchFlightOperation sfo = new SearchFlightOperation(logger);
				return sfo.executeOperation(parameters);
			} else if(operation.equals("book")) {
				BookFlightOperation bfo = new BookFlightOperation(logger);
				return bfo.executeOperation(parameters);
			} else if(operation.equals("cancel")) {
				CancelFlightOperation cfo = new CancelFlightOperation(logger);
				return cfo.executeOperation(parameters);
			} else if(operation.equals("show")) {
				ShowFlightsOperation sfo = new ShowFlightsOperation(logger);
				return sfo.executeOperation(parameters);
			}
		} catch (Exception e) {
			logger.log("Problem in executing the " + operation + " operation - " + e.getMessage());
			return "Problem in executing " + operation + " operation";
		}
		return "Invalid Operation - " + operation;
	}

	@Override
	public String handleRequest(Request input, Context context) {
		context.getLogger().log("Input: " + input);

		try {
			String operation = input.getOperation();
			Parameters parameters = input.getParameters();
			return executeRequestedOperation(operation, parameters, context.getLogger());
		} catch (JSONException e) {
			context.getLogger().log("Problem in executing operation - " + e.getMessage());
			return "Something went wrong while processing this query !!";
		}
	}
}
