package in.ac.iitk.cse.airlinedemobot.operations;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import in.ac.iitk.cse.airlinedemobot.ChatHandler;
import in.ac.iitk.cse.airlinedemobot.Parameters;

public class SearchFlightOperation implements Operation {

	private static String FLIGHT_DETAILS_FILE_PATH = "resources/FlightDetails.csv";
	
	private LambdaLogger logger;

	public SearchFlightOperation(LambdaLogger logger) {
		this.logger = logger;
	}

	@Override
	public String executeOperation(Parameters parameters) {
		try {
			parameters.capitaliseAllStringParameters();
			String src = parameters.getSrc();
			String dest = parameters.getDest();
			String time = parameters.getTime();
			String date = parameters.getDate();

			if(src == null || dest == null || date == null) {
				logger.log("Source, Destination and Date are required inputs");
				return produceOutput(null, null, true);
			}


			List<String[]> allFlights = readFlightDetails();
//			logger.log("Total flights: " + allFlights.size());

			List<String[]> searchedFlights = allFlights.parallelStream()
					.filter((flight) -> flight[0].equals(src) && flight[1].equals(dest))
					.filter((flight) -> time == null ? true : flight[2].equals(time))
					.map((flight) -> flight[2].equals("DAY") ? new String[] {flight[0], flight[1], "9 AM", flight[3]} : new String[] {flight[0], flight[1], "9 PM", flight[3]})
					.collect(Collectors.toList());
//			logger.log("Filtered flights: " + searchedFlights.size());

			return produceOutput(date, searchedFlights, false);
		} catch (Exception e) {
			logger.log("Problem in executing search operation - " + e.getMessage());
			return produceOutput(null, null, true);
		}
	}

	private String produceOutput(String date, List<String[]> searchedFlights, boolean error) {
		String response = "";
		if(error)
			response = "Something went wrong while processing this query !!";
		else if(searchedFlights.size() == 0)
			response = "Sorry, we don't have any flights matching your request !!";
		else {
			response += "Available flights for " + date + "\n";
			response += String.format("%14s | %14s | %6s | %s\n", "Source", "Destination", "Time", "Flight Number");
			for(String[] flight : searchedFlights)
				response += String.format("%14s | %14s | %6s | %s\n", flight[0], flight[1], flight[2], flight[3]);
		}
		return response;
	}

	private List<String[]> readFlightDetails() throws IOException, CsvException {
		InputStream in = ChatHandler.class.getResourceAsStream(FLIGHT_DETAILS_FILE_PATH);
		if(in == null)
			in = ChatHandler.class.getClassLoader().getResourceAsStream(FLIGHT_DETAILS_FILE_PATH);

		if(in == null)
			throw new IllegalStateException("Cannot read Flight details file");

		InputStreamReader is = new InputStreamReader(in);
		CSVReader csvReader = new CSVReader(is);
		csvReader.skip(1);
		List<String[]> allData = csvReader.readAll();
		is.close();
		csvReader.close();

		return allData;
	}
}
