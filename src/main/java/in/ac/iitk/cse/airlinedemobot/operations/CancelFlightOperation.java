package in.ac.iitk.cse.airlinedemobot.operations;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import in.ac.iitk.cse.airlinedemobot.ChatHandler;
import in.ac.iitk.cse.airlinedemobot.Parameters;

public class CancelFlightOperation implements Operation {

	private static String AWS_FILE_PATH = "resources/aws.properties";

	private static String BOOKING_FILE_NAME = "bookings.csv";

	private LambdaLogger logger;

	private Properties properties;

	private File tempFile;

	public CancelFlightOperation(LambdaLogger logger) throws IOException {
		this.logger = logger;
		tempFile = File.createTempFile("bookings", "temp");
		tempFile.deleteOnExit();
	}

	@Override
	public String executeOperation(Parameters parameters) {
		try {
			String bookingId = parameters.getBookingId();
			String email = parameters.getEmail();

			if(bookingId == null || email == null) {
				logger.log("Booking Id and Email are required inputs");
				return produceOutput(0, true, false, false);
			}

			if(!loadProperties())
				throw new IllegalStateException("Cannot load AWS configuration");
			
			final AmazonS3 s3 = getS3Client();
			List<String[]> allBookings = readAllBookings(s3);

			List<String[]> filteredBookings = allBookings.parallelStream()
					.filter((booking) -> booking[0].equals(bookingId))
					.collect(Collectors.toList());
			if(filteredBookings.size() == 0)
				return produceOutput(Integer.parseInt(bookingId), false, true, false);
			else {
				String[] booking = filteredBookings.get(0);

				if(booking[1].equals(email)) {
					removeBookingFromBookingsFile(allBookings, booking);
					s3.putObject(properties.getProperty("bucket_name"), BOOKING_FILE_NAME, tempFile);
					return produceOutput(Integer.parseInt(bookingId), false, false, false);
				}
				else
					return produceOutput(0, false, false, true);
			}
		} catch (Exception e) {
			logger.log("Problem in executing cancel operation - " + e.getMessage());
			return produceOutput(0, true, false, false);
		}
	}

	private AmazonS3 getS3Client() {
		AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard();
		AWSCredentials credentials = new BasicAWSCredentials(properties.getProperty("aws_access_key_id"), properties.getProperty("aws_secret_access_key"));
		builder.withCredentials(new AWSStaticCredentialsProvider(credentials));
		builder.withRegion(properties.getProperty("region"));
		return builder.build();
	}

	private boolean loadProperties() {
		try {
			InputStream in = ChatHandler.class.getResourceAsStream(AWS_FILE_PATH);
			if(in == null)
				in = ChatHandler.class.getClassLoader().getResourceAsStream(AWS_FILE_PATH);

			if(in == null)
				throw new IllegalStateException("Cannot read AWS credentials file");

			properties = new Properties();

			properties.load(in);

			return true;
		} catch (IOException e) {
			logger.log("Problem in loading Amazon properties - " + e.getMessage());
			return false;
		}
	}
	
	private String produceOutput(int bookingId, boolean error, boolean notFound, boolean emailMismatch) {
		if(error)
			return "Something went wrong while processing this query !!";
		else if(notFound)
			return "Sorry, we can't find any booking with Booking Id " + bookingId;
		else if(emailMismatch)
			return "This booking is not done with the supplied email !!";
		else
			return "Booking with Booking Id " + bookingId + " cancelled.";
	}

	private List<String[]> readAllBookings(AmazonS3 s3) throws Exception {
		S3Object o = s3.getObject(properties.getProperty("bucket_name"), BOOKING_FILE_NAME);
		S3ObjectInputStream s3is = o.getObjectContent();
		FileOutputStream fos = new FileOutputStream(tempFile);
		byte[] read_buf = new byte[1024];
		int read_len = 0;
		while ((read_len = s3is.read(read_buf)) > 0) {
			fos.write(read_buf, 0, read_len);
		}
		s3is.close();
		fos.close();

		CSVReader csvReader = new CSVReader(new FileReader(tempFile));
		List<String[]> allData = csvReader.readAll();
		csvReader.close();

		return allData;
	}
	
	private void removeBookingFromBookingsFile(List<String[]> allBookings, String[] bookingToRemove) throws IOException {
		allBookings.remove(bookingToRemove);
		allBookings.sort(new Comparator<String[]>() {
			@Override
			public int compare(String[] o1, String[] o2) {
				int i1 = Integer.parseInt(o1[0].toString());
				int i2 = Integer.parseInt(o2[0].toString());
				return i1 - i2;
			}
		});
		FileWriter fw = new FileWriter(tempFile, false);
		CSVWriter csvWriter = new CSVWriter(fw);
		csvWriter.writeAll(allBookings);
		csvWriter.close();
	}
}
