package in.ac.iitk.cse.airlinedemobot.operations;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import in.ac.iitk.cse.airlinedemobot.ChatHandler;
import in.ac.iitk.cse.airlinedemobot.Parameters;

public class ShowFlightsOperation implements Operation {

	private static String AWS_FILE_PATH = "resources/aws.properties";

	private static String BOOKING_FILE_NAME = "bookings.csv";

	private static String FLIGHT_DETAILS_FILE_PATH = "resources/FlightDetails.csv";

	private LambdaLogger logger;

	private Properties properties;
	
	private File tempFile;

	public ShowFlightsOperation(LambdaLogger logger) throws IOException {
		this.logger = logger;
		tempFile = File.createTempFile("bookings", "temp");
		tempFile.deleteOnExit();
	}

	@Override
	public String executeOperation(Parameters parameters) {
		try {
			String email = parameters.getEmail();
			String date = parameters.getDate();

			if(email == null) {
				logger.log("Email is the required input");
				return produceOutput(null, null, true);
			}

			if(!loadProperties())
				throw new IllegalStateException("Cannot load AWS configuration");
			
			final AmazonS3 s3 = getS3Client();
			if(s3 == null)
				throw new IllegalStateException("Cannot instantiate Amazon S3 instance");
			List<String[]> allBookings = readAllBookings(s3);

			List<String[]> selectedBookings = allBookings.parallelStream()
					.filter((booking) -> booking[1].equals(email))
					.filter((booking) -> date == null ? true : booking[3].equals(date))
					.collect(Collectors.toList());

			if(selectedBookings.size() == 0)
				return produceOutput(null, Collections.emptyList(), false);
			else {
				List<String[]> allFlights = readFlightDetails();

				/* Create a map that maps a flight number to the flight details (source, destination and time) */
				final Map<String, String[]> flightDetailsMap = 
						allFlights.parallelStream()
						.map((flight) -> flight[2].equals("DAY") ? new String[] {flight[0], flight[1], "9 AM", flight[3]} : new String[] {flight[0], flight[1], "9 PM", flight[3]})
						.map((flight) -> 
						new AbstractMap.SimpleEntry<String, String[]>(flight[3], Arrays.copyOfRange(flight, 0, 3)))
						.collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

				/* Create an extended list containing more details for each booking */
				List<String[]> selectedBookingsWithDetails = 
						selectedBookings.parallelStream()
						.map((booking) -> {
							String[] bookingWithDetails = new String[7];
							String[] additionalDetails = flightDetailsMap.get(booking[2]);
							// Put details in order
							bookingWithDetails[0] = booking[0];
							bookingWithDetails[1] = booking[3];
							bookingWithDetails[2] = additionalDetails[2];
							bookingWithDetails[3] = additionalDetails[0];
							bookingWithDetails[4] = additionalDetails[1];
							bookingWithDetails[5] = booking[2];
							bookingWithDetails[6] = booking[4];
							return bookingWithDetails;
						})
						.collect(Collectors.toList());

				return produceOutput(email, selectedBookingsWithDetails, false);
			}
		} catch (Exception e) {
			logger.log("Problem in executing show operation - " + e.getMessage());
			return produceOutput(null, null, true);
		}
	}

	private AmazonS3 getS3Client() {
		AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard();
		AWSCredentials credentials = new BasicAWSCredentials(properties.getProperty("aws_access_key_id"), properties.getProperty("aws_secret_access_key"));
		builder.withCredentials(new AWSStaticCredentialsProvider(credentials));
		builder.withRegion(properties.getProperty("region"));
		return builder.build();
	}

	private boolean loadProperties() {
		try {
			InputStream in = ChatHandler.class.getResourceAsStream(AWS_FILE_PATH);
			if(in == null)
				in = ChatHandler.class.getClassLoader().getResourceAsStream(AWS_FILE_PATH);

			if(in == null)
				throw new IllegalStateException("Cannot read AWS credentials file");

			properties = new Properties();

			properties.load(in);

			return true;
		} catch (IOException e) {
			logger.log("Problem in loading Amazon properties - " + e.getMessage());
			return false;
		}
	}

	private String produceOutput(String email, List<String[]> selectedBookingsWithDetails, boolean error) {
		String response = "";
		if(error)
			response = "Something went wrong while processing this query !!";
		else if(selectedBookingsWithDetails.size() == 0)
			response = "No bookings to show !!";
		else {
			response += "Bookings for email " + email + ":\n";
			response += String.format("%12s | %12s | %6s | %14s | %14s | %8s | %s\n", "Booking Id", "Date", "Time", "Source", "Destination", "Flight Number", "Number of Tickets");
			for(String[] flightDetails : selectedBookingsWithDetails)
				response += String.format("%12s | %12s | %6s | %14s | %14s | %8s | %s\n", flightDetails[0], flightDetails[1], flightDetails[2], flightDetails[3], flightDetails[4], flightDetails[5], flightDetails[6]);
		}
		return response;
	}

	private List<String[]> readAllBookings(AmazonS3 s3) throws Exception {
		S3Object o = s3.getObject(properties.getProperty("bucket_name"), BOOKING_FILE_NAME);
		S3ObjectInputStream s3is = o.getObjectContent();
		FileOutputStream fos = new FileOutputStream(tempFile);
		byte[] read_buf = new byte[1024];
		int read_len = 0;
		while ((read_len = s3is.read(read_buf)) > 0) {
			fos.write(read_buf, 0, read_len);
		}
		s3is.close();
		fos.close();

		CSVReader csvReader = new CSVReader(new FileReader(tempFile));
		List<String[]> allData = csvReader.readAll();
		csvReader.close();

		return allData;
	}

	private List<String[]> readFlightDetails() throws IOException, CsvException {
		InputStream in = ChatHandler.class.getResourceAsStream(FLIGHT_DETAILS_FILE_PATH);
		if(in == null)
			in = ChatHandler.class.getClassLoader().getResourceAsStream(FLIGHT_DETAILS_FILE_PATH);

		if(in == null)
			throw new IllegalStateException("Cannot read Flight details file");

		InputStreamReader is = new InputStreamReader(in);
		CSVReader csvReader = new CSVReader(is);
		csvReader.skip(1);
		List<String[]> allData = csvReader.readAll();
		is.close();
		csvReader.close();

		return allData;
	}
}
