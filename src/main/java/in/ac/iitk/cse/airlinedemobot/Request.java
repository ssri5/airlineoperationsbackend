package in.ac.iitk.cse.airlinedemobot;

import java.io.Serializable;

public class Request implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String operation;
	
	private Parameters parameters;

	public String getOperation() {
		return operation;
	}

	public Parameters getParameters() {
		return parameters;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}

	@Override
	public String toString() {
		return "Request [operation=" + operation + ", parameters=" + parameters + "]";
	}
}
