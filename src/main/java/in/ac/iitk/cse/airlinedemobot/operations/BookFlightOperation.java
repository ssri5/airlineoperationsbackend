package in.ac.iitk.cse.airlinedemobot.operations;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;

import in.ac.iitk.cse.airlinedemobot.ChatHandler;
import in.ac.iitk.cse.airlinedemobot.Parameters;

public class BookFlightOperation implements Operation {

	private static String AWS_FILE_PATH = "resources/aws.properties";

	private static String BOOKING_FILE_NAME = "bookings.csv";

	private static String FLIGHT_DETAILS_FILE_PATH = "resources/FlightDetails.csv";

	private LambdaLogger logger;

	private Properties properties;

	private File tempFile;

	public BookFlightOperation(LambdaLogger logger) throws Exception {
		this.logger = logger;
		tempFile = File.createTempFile("bookings", "temp");
		tempFile.deleteOnExit();
	}

	private void addBookingToBookingsFile(int bookingId, String email, String flightNumber, String date, Double numberOfTickets) throws IOException {
		FileWriter fw = new FileWriter(tempFile, true);
		CSVWriter csvWriter = new CSVWriter(fw);
		String[] line = new String[] {"" + bookingId, email, flightNumber, date, "" + numberOfTickets.intValue()};
		csvWriter.writeNext(line);
		csvWriter.close();
	}

	@Override
	public String executeOperation(Parameters parameters) {
		try {
			String flightNumber = parameters.getFlightNumber();
			Double numberOfTickets = parameters.getNumberOfTickets();
			String date = parameters.getDate();
			String email = parameters.getEmail();

			if(flightNumber == null || date == null || email == null) {
				logger.log("Flight Number, Email and Date are required inputs");
				return produceOutput(0, 0, null, null, true);
			}

			flightNumber = flightNumber.toUpperCase();
			if(!readFlightNumbers().contains(flightNumber))
				return produceOutput(0, 0, null, null, false);

			if(numberOfTickets == null || numberOfTickets == 0)
				numberOfTickets = 1d;

			if(!loadProperties())
				throw new IllegalStateException("Cannot load AWS configuration");

//			logger.log("Finding maximum booking id");

			final AmazonS3 s3 = getS3Client();
			int bookingId = readLastBookingId(s3) + 1;

//			logger.log("The booking id for the new booking is " + bookingId);

			addBookingToBookingsFile(bookingId, email, flightNumber, date, numberOfTickets);

			s3.putObject(properties.getProperty("bucket_name"), BOOKING_FILE_NAME, tempFile);

			return produceOutput(bookingId, numberOfTickets.intValue(), flightNumber, date, false);
		} catch (Exception e) {
			logger.log("Problem in executing book operation - " + e.getMessage());
			return produceOutput(0, 0, null, null, true);
		}
	}

	private AmazonS3 getS3Client() {
		AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard();
		AWSCredentials credentials = new BasicAWSCredentials(properties.getProperty("aws_access_key_id"), properties.getProperty("aws_secret_access_key"));
		builder.withCredentials(new AWSStaticCredentialsProvider(credentials));
		builder.withRegion(properties.getProperty("region"));
		return builder.build();
	}

	private boolean loadProperties() {
		try {
			InputStream in = ChatHandler.class.getResourceAsStream(AWS_FILE_PATH);
			if(in == null)
				in = ChatHandler.class.getClassLoader().getResourceAsStream(AWS_FILE_PATH);

			if(in == null)
				throw new IllegalStateException("Cannot read AWS credentials file");

			properties = new Properties();

			properties.load(in);

			return true;
		} catch (IOException e) {
			logger.log("Problem in loading Amazon properties - " + e.getMessage());
			return false;
		}
	}

	private String produceOutput(int bookingId, int numberOfTickets, String flightNumber, String date, boolean error) {
		if(error)
			return "Something went wrong while processing this query !!";
		else if(bookingId == 0)
			return "Sorry, we don't have any flight with the requested Flight Number";
		else
			return "Booked " + numberOfTickets + " ticket(s) on " + flightNumber + " for " + date + ". Note down your Booking Id: " + bookingId;
	}

	private List<String> readFlightNumbers() throws IOException, CsvException {
		InputStream in = ChatHandler.class.getResourceAsStream(FLIGHT_DETAILS_FILE_PATH);
		if(in == null)
			in = ChatHandler.class.getClassLoader().getResourceAsStream(FLIGHT_DETAILS_FILE_PATH);

		if(in == null)
			throw new IllegalStateException("Cannot read FlightDetails.csv file");

		InputStreamReader is = new InputStreamReader(in);
		CSVReader csvReader = new CSVReader(is);
		csvReader.skip(1);
		List<String[]> allData = csvReader.readAll();
		is.close();
		csvReader.close();

		return allData.parallelStream()
				.map((flight) -> flight[3])
				.collect(Collectors.toList());
	}

	private int readLastBookingId(AmazonS3 s3) throws Exception {
		S3Object o = s3.getObject(properties.getProperty("bucket_name"), BOOKING_FILE_NAME);
		S3ObjectInputStream s3is = o.getObjectContent();
		FileOutputStream fos = new FileOutputStream(tempFile);
		byte[] read_buf = new byte[1024];
		int read_len = 0;
		while ((read_len = s3is.read(read_buf)) > 0) {
			fos.write(read_buf, 0, read_len);
		}
		s3is.close();
		fos.close();

		CSVReader csvReader = new CSVReader(new FileReader(tempFile));
		List<String[]> allData = csvReader.readAll();
		csvReader.close();

		return Integer.parseInt(allData.get(allData.size()-1)[0]);
	}
}
